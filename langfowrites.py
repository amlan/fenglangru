# -*- coding: UTF-8 -*-
# -------------------------------------------------------------------------------
# Name:        Langfo
# Purpose:     Скрипт для перевода FengOffice
#
# Author:      Popov_VV
# Email:       pvv@amlan.ru
# Created:     09.03.2014
# Copyright:   (c) Popov_VV 2014
# Licence:     free
# -------------------------------------------------------------------------------

import re
import sys
import codecs


def main():
    input_files = ["language/en_us/_config.php",
                   "language/en_us/actions.php",
                   "language/en_us/administration.php",
                   "language/en_us/calendar.php",
                   "language/en_us/countries.php",
                   "language/en_us/emails.php",
                   "language/en_us/errors.php",
                   "language/en_us/fields.php",
                   "language/en_us/general.php",
                   "language/en_us/help.php",
                   "language/en_us/lang.js",
                   "language/en_us/messages.php",
                   "language/en_us/objects.php",
                   "language/en_us/project_interface.php",
                   "language/en_us/reporting.php",
                   "language/en_us/site_interface.php",
                   "language/en_us/slimey.js",
                   "language/en_us/timezones.php",
                   "language/en_us/upgrade.php",
                   "language/en_us/administration.php",
                   "plugins/core_dimensions/language/en_us/lang.php",
                   "plugins/mail/language/en_us/lang.php",
                   "plugins/workspaces/language/en_us/lang.php"]
    translated_lang = "ru_ru"  # На какой язык переводим, при исходном en_us
    for input_file in input_files:
        lang_files = "./edit/" + input_file.replace("en_us", translated_lang)  # Путь к файлам с переведенными фразами
        lang_dict = read_of_dict(lang_files + '.txt')  # Словарь {переменная:перевод}
        template_lang_file = "./FO/" + input_file  # Путь к файлу-шаблону
        finish_files = "./finish/" + input_file.replace("en_us", translated_lang)  # Путь к переведенным файла
        write_lang_file(template_lang_file, finish_files, lang_files, lang_dict)


def read_of_dict(r_file):  # Чтение из файла
    """Чтение из файла r_file перевода, возвращает словарь вида {Переменная:Перевод}"""
    try:
        file_in = codecs.open(r_file, 'r', encoding='UTF8')
        all_string = file_in.readlines()
        file_in.close()
    except:
        print u"Файл {0} неоткрывается!!!".format(r_file)
        sys.exit(1)
    else:
        translate_dict = {}
        for line in all_string:
            if re.search("(.+?)\t(.*)", line):
                translate_dict[re.search("(.+?)\t(.*)", line).group(1)] = re.search("(.+?)\t(.*)", line).group(2)
    return translate_dict  # Возвращаем словарь с переводом


def write_lang_file(r_file, w_file, lang_files, lang_dict, coding='UTF8'):
    """Запись в файл w_file перевода из файла r_file с подстановкой из словаря lang_dict,
       не переведенные фразы помещаем в файл lang_files"""
    try:
        file_in = codecs.open(r_file, 'r', coding)
        all_string = file_in.readlines()
        file_in.close()
        file_out = codecs.open(w_file, 'w', coding)
        not_translated_str = ""
        for line in all_string:
            # Также будут пропущены строки перевода с переносом строк.
            # "\'" - Пропускается в регулярке
            re_lang = re.search("(\'|\")(.+?)(\'|\")\s*(\:|\=\>)\s*(\'|\")(.+)(?<!\\\\)(\'|\")", line, re.DOTALL)
            if re_lang:  # Является ли эта строка переводом фразы?
                if re_lang.group(2) in lang_dict:  # есть ли для этой фразы перевод?
                    if line.count(re_lang.group(6)) > 1:
                        a = line.rfind(re_lang.group(6))
                        line = line[:a] + line[a:].replace(re_lang.group(6), lang_dict[re_lang.group(2)])
                    else:
                        line = line.replace(re_lang.group(6), lang_dict[re_lang.group(2)])
                else:  # У фразы нет перевода, сохраняем в переменную для последующего сохранения в файл
                    not_translated_str += re_lang.group(2) + u"\t" + re_lang.group(6) + u"\n"
            else:
                if re.search("(\'|\")(.+?)(\'|\")\s*(\:|\=\>)\s*(\'|\")(.+)", line, re.DOTALL):
                    not_translated_str += u"Не переведено из за переносов строки в оригинальном файле" + line + u"\n"
            file_out.write(line)
        file_out.close()

        if len(not_translated_str):  # Если есть не переведенные данные, то записываем их
            file_with_not_translated_string = codecs.open(lang_files + ".err.txt", 'w', coding)
            file_with_not_translated_string.write(not_translated_str)
            file_with_not_translated_string.close()
    except:
        print u"Файл {0} не сохраняется!!!".format(w_file)
        sys.exit(1)


if __name__ == '__main__':
    main()
