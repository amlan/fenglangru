<?php

  // Actions
  return array(
  
    // Registration
    'register' => 'Регистрация',
    'login' => 'Вход',
    'logout' => 'Выход',
    'hide welcome info' => '&raquo; Cкрыть эту информацию',
    
    // Companies
    'add company' => 'Добавить компанию',
    'edit company' => 'Редактировать компанию',
    'delete company' => 'Удалить компанию',
    'edit company logo' => 'Обновить логотип',
    'delete company logo' => 'Удалить логотип',
    
    // Clients
    'add client' => 'Добавить клиента',
    'edit client' => 'Редактировать контакт',
    'delete client' => 'Удалить контакт',
    
    // Users
    'add user' => 'Добавить пользователя',
    'edit user' => 'Редактировать пользователя',
    'delete user' => 'Удалить пользователя',
    'create user from contact' => 'Создать пользователя из контакта',
    
    // Group
    'add group' => 'Добавить группу',
    'edit group' => 'Редактировать группу',
    'delete group' => 'Удалить группу',
    
    // Project
    'add project' => 'Добавить проект',
    'edit project' => 'Редактировать проект',
    'delete project' => 'Удалить проект',
    'mark project as finished' => 'Пометить проект как закрытый',
    'mark project as active' => 'Пометить проект как активный',
    
    // Workspace
    'add workspace' => 'Добавить Проект',
    'edit workspace' => 'Редактировать Проект',
    'delete workspace' => 'Удалить Проект',
    'mark workspace as finished' => 'Пометить Проект как завершенный',
    'mark workspace as active' => 'Пометить Проект как активный',
    
    // Messages
    'add message' => 'Добавить заметку',
    'add new message' => 'Добавить новую заметку',
    'edit message' => 'Редактировать заметку',
    'delete message' => 'Удалить заметку',
    'view message' => 'Просмотреть заметку',
    'update message options' => 'Обновить настройки заметок',
    'subscribe to object' => 'Подписаться',
    'unsubscribe from object' => 'Отписаться',
    
    // Comments
    'add comment' => 'Добавить комментарий',
    'edit comment' => 'Редактировать комментарий',
    
    // Task list
    'add task list' => 'Создать задачу',
    'edit task list' => 'Редактировать список задач',
    'delete task list' => 'Удалить список задач',
    'reorder tasks' => 'Упорядочить задачи',
	'reorder sub tasks' => 'Упорядочить подзадачи',
  	'copy task' => 'Создать копию задачи',
  	'copy milestone' => 'Создать копию этапа',
    
    // Task
    'add task' => 'Добавить задачу',
	'add sub task' => 'Добавить подзадачу',
    'edit task' => 'Редактировать задачу',
    'delete task' => 'Удалить задачу',
    'mark task as completed' => 'Пометить задачу как завершенную',
    'mark task as open' => 'Пометить задачу как открытую',
    'apply milestone to subtasks' => 'Применить этап к подзадачам',
  	'apply workspace to subtasks' => 'Применить Проект к подзадачам',
  	  
    // Milestone
    'add milestone' => 'Добавить этап',
    'edit milestone' => 'Редактировать этап',
    'delete milestone' => 'Удалить этап',
    
    // Events
    'add event' => 'Добавить событие',
    'edit event' => 'Редактировать событие',
    'delete event' => 'Удалить событие',
    'previous month' => 'Предыдущий месяц',
    'next month' => 'Следующий месяц',
    'previous week' => 'Предыдущая неделя',
    'next week' => 'Следующая неделя',
    'previous day' => 'Предыдущий день',
    'next day' => 'Следующий день',
    'back to calendar' => 'Назад к календарю',
    'back to day' => 'Назад к дням',
    'pick a date' => 'Выберите дату',
    'month' => 'Месяц',
    'week' => 'Неделя',
    
    //Groups
    'can edit company data' => 'Разрешить редактировать информацию о компании',
    'can manage security' => 'Разрешить управление безопасностью',
    'can manage workspaces' => 'Разрешить управление Проектами',
    'can manage configuration' => 'Разрешить изменять конфигурацию ',
    'can manage contacts' => 'Разрешить управление контактами',
    'group users' => 'Группы пользователей',
    
    
    // People
    'update people' => 'Обновить',
    'remove user from project' => 'Удалить пользователя из Проекта',
    'remove company from project' => 'Удалить компанию из Проекта',
    
    // Password
    'update profile' => 'Обновить профиль',
    'change password' => 'Изменить пароль',
    'update avatar' => 'Обновить Аватар',
    'delete current avatar' => 'Удалить текущий аватар',
    
    // Forms
    'add form' => 'Добавить форму',
    'edit form' => 'Редактировать форму',
    'delete form' => 'Удалить форму',
    'submit project form' => 'Подтвердить',
    
    // Files
    'add file' => 'Добавить файл',
    'edit file properties' => 'Редактировать свойства файла',
    'upload file' => 'Загрузить файл',
    'create new revision' => 'Создать новую версию',
  	'upload new revision' => 'Загрузить новую версию',

    'add document' => 'Добавить документ',
    'save document' => 'Сохранить документ',
    'add spreadsheet' => 'Добавить таблицу',
    'add presentation' => 'Добавить презентацию',
    'document' => 'Документ',
    'spreadsheet' => 'Таблица',
    'presentation' => 'Презентация',

    'new' => 'Новый',
    'upload' => 'Загрузить',
    'hide' => 'Скрыть',
	'show' => 'Показать',
    'new document' => 'Новый документ',
    'new spreadsheet' => 'Новая таблица',
    'new presentation' => 'Новая презентация',

    'slideshow' => 'Слайдшоу',
    'revisions and comments' =>'Версии и Комментарии',
        
    'Save' => 'Сохранить',
    'all elements' => 'Все элементы',
    'collapse all' => 'Свернуть всё',
    'expand all' => 'Развернуть всё',

    'properties' => 'Свойства',
    'edit file' => 'Редактировать файл',
    'edit document' => 'Редактировать документ',
    'edit spreadsheet' => 'Редактировать таблицу',
    'edit presentation' => 'Редактировать презентацию',

  	'play' => 'Воспроизвести',
  	'queue' => 'Поставить в очередь',
  
    'delete file' => 'Удалить файл',
    
    'add folder' => 'Добавить папку',
    'edit folder' => 'Редактировать папку',
    'delete folder' => 'Удалить папку',
    
    'edit file revisions' => 'Редактировать версию',
    'version' => 'Версия',
    'last modification' => 'Последнее изменение',
    
    'link object' => 'Связать с объектом',
    'link objects' => 'Связать с объектами',
    'show all linked objects' => 'Показать все связанные объекты ({0})',
  	'link more objects' => 'Связать с еще одним объектом',
    'unlink' => 'Удалить связь',
    'unlink object' => 'Удалить связь с объектом',
    'unlink objects' => 'Удалить связи с объектами',
	'extract' => 'Извлечь',
  	'add files to zip' => 'Добавить файлы в архив ZIP',
  	
    // Tags
    'delete tag'  => 'Удалить тег',
    
    // Permissions
    'update permissions' => 'Обновить права',
    'edit permissions' => 'Редактировать права',
    'edit permissions explanation'  => 'Пометьте нужные поля, чтобы дать доступ пользователю к недавно созданному Проекту.',
  
  	'save as new revision' => 'Сохранить как новую версию',
	'save as' => 'Сохранить',
  	'save with a new name' => 'Сохранить с новым именем',
	'details' => 'Подробнее',
	'view history for' => 'Просмотреть историю для',
	'view history' => 'Просмотреть историю',    
	'edit preferences' => 'Настройки',
	'view milestone' => 'Просмотреть этап',
  	'custom properties' => 'Дополнительные параметры',
  	'move to trash' => 'Удалить в корзину',
  	'restore from trash' => 'Восстановить из корзины',
  	'delete permanently' => 'Удалить навсегда',
  	'copy file' => 'Копировать файл',
  	'open weblink' => 'Открыть ссылку',
  
	'archive' => 'Архивировать',
 	'unarchive' => 'Разархивировать',
  	'confirm archive selected objects' => 'Вы действительно хотите архивировать выбранные объекты?',
	'confirm archive object' => 'Вы уверены, что хотите заархивировать этот объект?',
	'confirm unarchive object' => 'Вы действительно хотите разархивировать этот объект?',
  	'archived' => 'Заархивирован',
	'confirm archive member' => 'Вы действительно хотите архивировать {0}? Это так же заархивирует все объекты в {0}.',
	'confirm unarchive member' => 'Вы действительно хотите разархивировать {0}? Этим действием вы также разархивируете все объекты в {0}.',
	'success archive member' => '{0} был успешно заархивирован. {1} объектов из {0} были заархивированны.',
	'success unarchive member' => '{0} был успешно разархивирован. {1} объектов из {0} были разархивированны.',
  	
	'confirm delete permanently' => 'Вы уверенны, что хотите удалить {0}?',

  	'report as spam' => 'Пометить как спам',
	'not spam' => 'Не спам',
  
  	'mark as unread' => 'Пометить как непрочитанное',
  	'activate' => 'Активировать',
  	'disabled' => 'Отключить',
  	
  	'template context' => 'Выберите месторасположение шаблона',
      
        'config activity' => 'Настройка фильтра активности',
        'entry to see the dimension' => 'Войти, для просмотра размерности',
        'recent activities to show' => 'Показать последние действия',
  		'recent activities to show display' => 'Просмотр',
  		'recent activities to show lines' => 'строк',
        'views and downloads' => 'Показать просмотры и загрузки',
        'confirm delete permanently sync' => 'Вы уверены, что требуется удалить синхронизировнные данные?',
  ); // array

?>