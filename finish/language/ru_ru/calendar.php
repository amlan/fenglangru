<?php

  return array(
	// ########## QUERY ERRORS ###########
	"CAL_QUERY_GETEVENT_ERROR" => "Ошибка базы данных: не удалось найти событие по ID",
	"CAL_QUERY_SETEVENT_ERROR" => "Ошибка базы данных: не удалось записать данные события",
	// ########## SUBMENU ITEMS ###########
	"CAL_SUBM_LOGOUT" => "Выйти",
	"CAL_SUBM_LOGIN" => "Войти",
	"CAL_SUBM_ADMINPAGE" => "Страница администратора",
	"CAL_SUBM_SEARCH" => "Поиск",
	"CAL_SUBM_BACK_CALENDAR" => "Назад к календарю",
	"CAL_SUBM_VIEW_TODAY" => "Смотреть сегодняшние события",
	"CAL_SUBM_ADD" => "Добавить событие сегодня",
	// ########## NAVIGATION MENU ITEMS ##########
	"CAL_MENU_BACK_CALENDAR" => "Назад к календарю",
	"CAL_MENU_NEWEVENT" => "Новое событие",
	"CAL_MENU_BACK_EVENTS" => "Назад к событиям",
	"CAL_MENU_GO" => "Вперед",
	"CAL_MENU_TODAY" => "Сегодня",
	// ########## USER PERMISSION ERRORS ##########
	"CAL_NO_READ_PERMISSION" => "У вас нет полномочий просматривать это событие.",
	"CAL_NO_WRITE_PERMISSION" => "У вас нет полномочий добавлять или редактировать события.",
	"CAL_NO_EDITOTHERS_PERMISSION" => "У вас нет полномочий редактировать события других пользователей.",
	"CAL_NO_EDITPAST_PERMISSION" => "У вас нет полномочий добавлять или редактировать прошедшие события.",
	"CAL_NO_ACCOUNTS" => "Этот календарь не допускает модификации, вход только для администратора.",
	"CAL_NO_MODIFY" => "невозможно изменить",
	"CAL_NO_ANYTHING" => "У вас нет полномочий делать что-либо на этой странице",
	"CAL_NO_WRITE" => "У вас нет полномочий для создания событий",
	// ############ DAYS ############
	"CAL_MONDAY" => "Понедельник",
	"CAL_TUESDAY" => "Вторник",
	"CAL_WEDNESDAY" => "Среда",
	"CAL_THURSDAY" => "Четверг",
	"CAL_FRIDAY" => "Пятница",
	"CAL_SATURDAY" => "Суббота",
	"CAL_SUNDAY" => "Воскресенье",
	"CAL_SHORT_MONDAY" => "Пн",
	"CAL_SHORT_TUESDAY" => "Вт",
	"CAL_SHORT_WEDNESDAY" => "Ср",
	"CAL_SHORT_THURSDAY" => "Чт",
	"CAL_SHORT_FRIDAY" => "Пт",
	"CAL_SHORT_SATURDAY" => "Сб",
	"CAL_SHORT_SUNDAY" => "Вс",
	// ############ MONTHS ############
	"CAL_JANUARY" => "Январь",
	"CAL_FEBRUARY" => "Февраль",
	"CAL_MARCH" => "Март",
	"CAL_APRIL" => "Апрель",
	"CAL_MAY" => "Май",
	"CAL_JUNE" => "Июнь",
	"CAL_JULY" => "Июль",
	"CAL_AUGUST" => "Август",
	"CAL_SEPTEMBER" => "Сентябрь",
	"CAL_OCTOBER" => "Октябрь",
	"CAL_NOVEMBER" => "Ноябрь",
	"CAL_DECEMBER" => "Декабрь",
	
	
	
	
	
	
	// SUBMITTING/EDITING EVENT SECTION TEXT (event.php)
	"CAL_MORE_TIME_OPTIONS" => "Дополнительные настройки времени",
	"CAL_REPEAT" => "Повторять",
	"CAL_EVERY" => "Каждый",
	"CAL_REPEAT_FOREVER" => "Повторять всегда",
	"CAL_REPEAT_UNTIL" => "Повторять до",
	"CAL_TIMES" => "раз(а)",
	"CAL_HOLIDAY_EXPLAIN" => "Событие будет повторяться каждый",
	"CAL_DURING" => "Во время",
	"CAL_EVERY_YEAR" => "Каждый год",
	"CAL_HOLIDAY_EXTRAOPTION" => "Или, поскольку событие попадает на последнюю неделю месяца, пометьте здесь, чтобы оно было записано ПОСЛЕДНИМ",
	"CAL_IN" => "в",
	"CAL_PRIVATE_EVENT_EXPLAIN" => "Это частное событие",
	"CAL_SUBMIT_ITEM" => "Утвердить пункт",
	"CAL_MINUTES" => "Минут", 
	"CAL_MINUTES_SHORT" => "мин",
	"CAL_TIME_AND_DURATION" => "Дата, время и длительность",
	"CAL_REPEATING_EVENT" => "Повторяющееся событие",
	"CAL_EXTRA_OPTIONS" => "Дополнительные настройки",
	"CAL_ONLY_TODAY" => "Только в этот день",
	"CAL_DAILY_EVENT" => "Повторять ежедневно",
	"CAL_WEEKLY_EVENT" => "Повторять еженедельно",
	"CAL_MONTHLY_EVENT" => "Повторять ежемесячно",
	"CAL_YEARLY_EVENT" => "Повторять ежегодно",
	"CAL_HOLIDAY_EVENT" => "Повторять по выходным",
	"CAL_UNKNOWN_TIME" => "Время начала неизвестно",
	"CAL_ADDING_TO" => "Добавлено к",
	"CAL_ANON_ALIAS" => "Псевдоним",
	"CAL_EVENT_TYPE" => "Тип события",
	
	// MULTI-SECTION RELATED TEXT (used by more than one section, but not everwhere)
	"CAL_DESCRIPTION" => "Описание", // (search, view date, view event)
	"CAL_DURATION" => "Период", // (view event, view date)
	"CAL_DATE" => "Дата", // (search, view date)
	"CAL_NO_EVENTS_FOUND" => "Событий не найдено", // (search, view date)
	"CAL_NO_SUBJECT" => "Нет темы", // (search, view event, view date, calendar)
	"CAL_PRIVATE_EVENT" => "Частное событие", // (search, view event)
	"CAL_DELETE" => "Удалить", // (view event, view date, admin)
	"CAL_MODIFY" => "Изменить", // (view event, view date, admin)
	"CAL_NOT_SPECIFIED" => "Не определено", // (view event, view date, calendar)
	"CAL_FULL_DAY" => "Весь день", // (view event, view date, calendar, submit event)
	"CAL_HACKING_ATTEMPT" => "Попытка взлома - IP адрес записан", // (delete)
	"CAL_TIME" => "Время", // (view date, submit event)
	"CAL_HOURS" => "Часы", // (view event, submit event)
	"CAL_HOUR" => "Час", // (view event, submit event)
	"CAL_ANONYMOUS" => "Анонимно", // (view event, view date, submit event),
	
	
	"CAL_SELECT_TIME" => "Выбрать время начала",
	
	'event invitations' => 'Приглашения на участие',
	'event invitations desc' => 'Пригласить выбранных людей участвовать в этом событии',
	'send new event notification' => 'Послать уведомление по почте',
	'new event notification' => 'Добавлены новые события',
    'change event notification' => 'Событие изменено',
	'deleted event notification' => 'Событие удалено',
	'attendance' => 'Вы будете участвовать?',
    'confirm attendance' => 'Подтвердить присутствие',
	'maybe' => 'Может быть',
    'decide later' => 'Решить позже',
    'view event' => 'Посмотреть событие',
	'new event created' => 'Создано новое событие',
	'event changed' => 'Событие изменено',
 	'event deleted' => 'Событие удалено',
	'calendar of' => 'Календарь для {0}',
	'all users' => 'Все пользователи',
  	'error delete event' => 'Ошибка удаления события',
  	'event invitation response' => 'Ответить на приглашение',
  	'user will attend to event' => '{0} примут участие в этом мероприятии',
  	'user will not attend to event' => '{0} не примут участие в этом мероприятии',
  	'also confirmed attendance' => 'Также подтвердил участие',
  	'awaiting confirmation' => 'Ожидает подтверждения',
    'rejected invitation' => 'Не будет участвовать', 
  	'accept or reject invitation help, click on one of the links below' => 'Чтобы принять или отклонить приглашение, нажмите ниже на одну из ссылок',
  	'accept invitation' => 'Принять приглашение',
	'reject invitation' => 'Отклонить приглашение',
  	'invitation accepted' => 'Приглашение принято',
	'invitation rejected' => 'Приглашение отклонено',
  	'who' => 'Кто',
  	'what' => 'Что',
  
	"days" => "день/дней(я)",
	"weeks" => "недели",
	"months" => "месяцы",
	"years" => "годы",

	'invitations' => 'Приглашения',
	'pending response' => 'В ожидании ответа',
	'participate' => 'Будут присутствовать',
 	'no invitations to this event' => 'Нет приглашений, отправленных в это событие',
	'duration must be at least 15 minutes' => 'Продолжительность должна быть не менее 15 минут',
  
	'event dnx' => 'Запрашиваемое событие не найдено',
	'no subject' => 'Нет темы',
	'success import events' => 'Импортировано событий: {0}',
	'no events to import' => 'Нет событий для импорта',
	'import events from file' => 'Импорт списка событий из файла',
	'file should be in icalendar format' => 'Файл должен быть в формате iCalendar',
	'export calendar' => 'Экспорт календаря',
	'range of events' => 'Диапазон событий',
	'from date' => 'С',
	'to date' => 'до',
	'success export calendar' => 'Экспортировано событий: {0}.',
	'calendar name desc' => 'Имя календаря для экспортирования',
	'calendar will be exported in icalendar format' => 'Календарь будет экспортирован в формате iCalendar.',
	'view date title' => 'l, d/m/y',

  	'copy this url in your calendar client software' => 'Скопируйте этот URL в вашем календаре, для того чтобы импортировать мероприятия из этого календаря',
	'import events from third party software' => 'Импорт мероприятий из сторонних программных',
	'subws' => 'доп.проект',
	'check to include sub ws' => 'Включить в доп.проект',
  	'week short' => 'Неделя',
  	'week number x' => 'Неделя {0}',
      
        'could not connect to calendar' => 'Невозможно подключиться к календарю.',
        'calendar sinchronization' => 'Синхронизация с Google Календарем.',
        'no calendars' => 'Нет календарей',
        'name calendar' => 'Имя',
        'users link' => 'Ссылка на календарь',       
        'account gmail' => 'Gmail аккаунт',
        'password gmail' => 'Gmail пароль',
        'add calendar' => 'Добавить календарь',
        'add account' => 'Добавить аккаунт',
        'new calendar' => 'Новый Google календарь',
        'edit calendar' => 'Редактировать Google календарь',
        'must enter a calendar name' => 'Необходимо ввести имя календаря',
        'must enter an calendar link' => 'Необходимо ввести ссылку на календарь',
        'success add calendar' => 'Календарь успешно создан',
        'success delete calendar' => 'Календарь успешно удален',
        'success edit calendar' => 'Календарь успешно изменен',
        'success add sync' => 'Успешная синхронизация',
        'success edit account gmail' => 'Gmail аккаунт успешно изменен',
        'success add account gmail' => 'Gmail аккаунт успешно создан',
        'must enter a account gmail' => 'Необходимо ввести Gmail аккаунт',
        'must enter the password gmail' => 'Необходимо ввести Gmail пароль',
        'untitle event' => 'Безымянное событие',
        'click sincronizar' => 'Нажмите здесь, если вы хотите чтобы Feng Office синхронизировал события с Google Календарем',
        'feng calendar' => 'Feng Office Календарь - {0}',
        'import calendars' => 'Импорт',
        'account has already' => 'Аккаунт уже был созддан ранее',
        'sync event feng' => 'Синхронизировать все события с Feng Office',
        'list calendar' => 'Вы хотите импортировать события с одного из ваших Google календарей?',
        'list calendar desc' => 'Пожалуйста, выберите календарь, из которого вы хотите импортировать события',      
        'check your account' => 'Проверьте ваш аккаунт',
        'success import calendar' => 'События были успешно импортированы',
      
        'all day event' => 'Весь день',
        'date' => 'Дата',
        'time' => 'Время',
  ); // array
?>