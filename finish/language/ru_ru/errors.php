<?php

  /**
  * Error messages
  *
  * @version 1.0
  * @author Ilija Studen <ilija.studen@gmail.com>
  */

  // Return langs
  return array(
  
    // General
    'invalid email address' => 'Неправильный формат E-mail адреса',
	'error invalid recipients' => 'Неверный адрес электронной почты, найденный в поле "{0}": {1}',
	'no context permissions to add' => 'У вас недостаточно прав на добавление {0} in {1}',
	'you must select where to keep' => 'Вы должны выбрать, где сохранить {0}.',

    // Company validation errors
    'company name required' => 'Требуется название компании',
    'company homepage invalid' => 'Формат адреса домашней страницы неверный',
    
    // User validation errors
    'username value required' => 'Необходимо ввести имя пользователя',
    'username must be unique' => 'Извините, но выбранное имя пользователя уже существует',
    'email value is required' => 'Необходимо ввести адрес Email',
    'email address must be unique' => 'Извините, но выбранный адрес Email уже существует',
    'company value required' => 'Пользователь должен принадлежать какой-либо компании или организации',
    'password value required' => 'Необходимо ввести пароль',
    'passwords dont match' => 'Значения пароля не совпадают',
    'old password required' => 'Требуется старый пароль',
    'invalid old password' => 'Старый пароль неверен',
    'users must belong to a company' => 'Контакты должны принадлежать компании, чтобы создать пользователя',
    'contact linked to user' => 'Контакты связаны с пользователем {0}',
  
  	// Password validation errors
  	'password invalid min length' => 'Длина пароля должна быть не менее {0} символов',
  	'password invalid numbers' => 'Пароль должен иметь по крайней мере {0} числовых символов',
  	'password invalid uppercase' => 'Пароль должен иметь по крайней мере {0} заглавных букв',
  	'password invalid metacharacters' => 'Пароль должен иметь по крайней мере {0} метасимволов',
  	'password exists history' => 'Пароль был уже использован в последних десяти паролях',
  	'password invalid difference' => 'Пароль должен отличаться, по крайней мере 3-я символами из последних 10 паролей',
  	'password expired' => 'Срок действия вашего пароля истек',
  	'password invalid' => 'Ваш пароль уже не действителен',
    
    // Avatar
    'invalid upload type' => 'Неверный тип файла. Допустимые типы файлов - {0}',
    'invalid upload dimensions' => 'Недопустимый размер изображения. Максимальный размер - {0}x{1} пикселей',
    'invalid upload size' => 'Недопустимый размер изображения. Максимальный размер - {0}',
    'invalid upload failed to move' => 'Не удалось переместить загруженный файл',
    
    // Registration form
    'terms of services not accepted' => 'Для того, чтобы создать счет, Вы должны прочитать и принять условия сервиса',
    
    // Init company website
    'failed to load company website' => 'Не удалось загрузить веб-сайт. Не найдена основная компания',
    'failed to load project' => 'Не удалось загрузить активный проект',
    
    // Login form
    'username value missing' => 'Пожалуйста, введите имя пользователя',
    'password value missing' => 'Пожалуйста, введите пароль',
    'invalid login data' => 'Не удалось войти в систему. Пожалуйста, проверьте имя пользователя и пароль и попробуйте снова',
    
    // Add project form
    'project name required' => 'Необходимо ввести имя проекта',
    'project name unique' => 'Имя проекта должно быть уникальным',
    
    // Add message form
    'message title required' => 'Необходимо ввести заголовок',
    'message title unique' => 'Заголовок не должен повторяться в одном проекте',
    'message text required' => 'Необходимо ввести текст',
    
    // Add comment form
    'comment text required' => 'Необходимо ввести текст комментария',
    
    // Add milestone form
    'milestone name required' => 'Необходимо ввести название этапа',
    'milestone due date required' => 'Необходимо ввести дату выполнения этапа',
    
    // Add task list
    'task list name required' => 'Необходимо ввести название задачи',
    'task list name unique' => 'Название задачи должно быть уникальным в проекте',
    'task title required' => 'Необходимо ввести заголовок задачи',
  
    // Add task
    'task text required' => 'Необходимо ввести текст задачи',
	'repeat x times must be a valid number between 1 and 1000' => 'Количество повторов должно быть действительным числом от 1 до 1000.',
	'repeat period must be a valid number between 1 and 1000' => 'Период повтора должен быть действительным числом от 1 до 1000.',
  	'to repeat by start date you must specify task start date' => 'Чтобы повторить на дату начала, вы должны указать дату запуска задания',
	'to repeat by due date you must specify task due date' => 'Что бы повторить по дате выполнения, вы должны указать дату выполнения',
	'task cannot be instantiated more times' => 'Задача не может быть обработана несколько раз, это последний повтор.',
	
    // Add event
    'event subject required' => 'Необходимо ввести тему события',
    'event description maxlength' => 'Описание не должно превышать 3000 символов',
    'event subject maxlength' => 'Тема может содержать до 100 символов',
    
    // Add project form
    'form name required' => 'Необходимо ввести название формы',
    'form name unique' => 'Название формы должно быть уникальным',
    'form success message required' => 'Необходима отметка успешного выполнения',
    'form action required' => 'Необходимо указать действие формы',
    'project form select message' => 'Пожалуйста, выберите заметку',
    'project form select task lists' => 'Пожалуйста, выберите задачу',
    
    // Submit project form
    'form content required' => 'Пожалуйста, вставьте содержимое в текстовое поле',
    
    // Validate project folder
    'folder name required' => 'Необходимо указать имя папки',
    'folder name unique' => 'Имя папки должно быть уникальным в этом проекте',
    
    // Validate add / edit file form
    'folder id required' => 'Пожалуйста, выберите папку',
    'filename required' => 'Необходимо ввести имя файла',
  	'weblink required' => 'Требуется Веб-ссылка',
    
    // File revisions (internal)
    'file revision file_id required' => 'Версия должна быть связана с файлом',
    'file revision filename required' => 'Необходимо ввести имя файла',
    'file revision type_string required' => 'Неизвестный тип файла',
    'file revision comment required' => 'Необходим комментарий для этой редакции',
    'there are no changes' => 'Изменений нет. Документ не будет перезаписан.',
    
    // Test mail settings
    'test mail recipient required' => 'Необходимо ввести адрес получателя',
    'test mail recipient invalid format' => 'Неверный формат адреса получателя',
    'test mail message required' => 'Необходимо ввести сообщение',
    
    // Mass mailer
    'massmailer subject required' => 'Необходимо ввести тему письма',
    'massmailer message required' => 'Необходимо ввести само сообщение',
    'massmailer select recepients' => 'Пожалуйста, выберите пользователей, которые получат Ваше сообщение',
    
  	//Email module
  	'mail account name required' => 'Необходимо ввести название учётной записи',
  	'mail account id required' => 'Необходимо ввести ID счета',
  	'mail account server required' => 'Требуется сервер',
  	'mail account password required' => 'Требуется пароль',
	'send mail error' => 'Ошибка при отправке почты. Возможно, неправильные SMTP настройки.',
    'email address already exists' => 'Этот адрес уже используется',
  
  	'session expired error' => 'Сессия закрыта в связи с отсутствием активности пользователя. Пожалуйста, зайдите снова',
  	'unimplemented type' => 'Не использованный тип',
  	'unimplemented action' => 'Не использованное действие',
  
  	'workspace own parent error' => 'Проект не может быть источником самому себе',
  	'task own parent error' => 'Задача не может быть источником сама себе',
  	'task child of child error' => 'Задача не может быть потомком одного из своих потомков',
  
  	'chart title required' => 'Необходимо ввести заголовок диаграммы.',
  	'chart title unique' => 'Заголовок диаграммы должен быть уникальным.',
    'must choose at least one workspace error' => 'Вы должны выбрать хотя бы один проект для помещения объекта в него.',
    
    
    'user has contact' => 'Контакт, присвоенный такому пользователю, уже существует',
    
    'maximum number of users reached error' => 'В системе зарегистрировано максимальное количество пользователей',
	'maximum number of users exceeded error' => 'Превышено максимально допустимое количество пользователей в системе. Приложение не сможет работать, пока Вы не исправите эту ситуацию.',
	'maximum disk space reached' => 'Ваша дисковая квота использована. Пожалуйста, удалите что-то, прежде чем добавлять новое, или обратитесь к администратору.',
    'name must be unique' => 'Извините, но выбранное имя уже используется',
  	'not implemented' => 'Не выполнено',
  	'return code' => 'Код возврата: {0}',
  	'task filter criteria not recognised' => 'Критерии фильтра \'{0}\' задачи не распознаны',
  	'mail account dnx' => 'Почтовый ящик не существует',
    'error document checked out by another user' => 'Этот документ заблокирован другим пользователем',
  	//Custom properties
  	'custom property value required' => '{0} требуется',
  	'value must be numeric' => 'Значение(я) для {0} должно(ы) быть числовым(и)',
  	'values cannot be empty' => 'Значение для {0} не должно быть пустым',
  
  	//Reports
  	'report name required' => 'Требуется название отчета',
  	'report object type required' => 'Требуется тип объекта для отчета',

  	'error assign task user dnx' => 'Попытка присвоить существующего пользователя',
	'error assign task permissions user' => 'Вы не имеете права назначать задания для этого пользователя',
	'error assign task company dnx' => 'Попытка присвоить существующую компанию',
	'error assign task permissions company' => 'Вы не имеете права назначать задания для этой компании',
  	'account already being checked' => 'Аккаунт уже проверен.',
  	'no files to compress' => 'Нет файлов для сжатия',
	'error cannot assign task to user' => '{0} не может быть задачей для правопреемника {1}.',
  
  	//Subscribers
  	
  	'cant modify subscribers' => 'Неудалось изменить подписчиков',
  	'this object must belong to a ws to modify its subscribers' => 'Этот объект должен принадлежать проекту, чтобы изменить его подписчиков.',

  	'mailAccount dnx' => 'Учетной записи электронной почты не существует',
  	'error add contact from user' => 'Не удалось добавить контакт с пользователем.',
  	'zip not supported' => 'ZIP не поддерживается сервером',
  	'no tag specified' => 'Тег не указан',
  
    'no mailAccount error' => 'Действие недопустимо. У вас нет почтовых аккаунтов.',
	'content too long not loaded' => 'Предыдущее почтовое содержание слишком длинное и не было загружено, но будет отправлен в этом письме.',
  	'member name already exists in dimension' => 'Элемент \'{0}\' уже существует в выбранной области.',  
	'must choose at least one member of' => 'Вы должны выбрать хотя бы один из членов {0}.',
	'timeslot dnx' => 'Временной интервал не существует',
	'you dont have permissions to classify object in member' => 'У Вас нет разрешения присвоить для \'{0}\' тег\проект \'{1}\'',
  
  	'uploaded file bigger than max upload size' => 'Размер загружаемого документа больше, чем разрешенный лимит: {0}.',
  	'date format error' => 'Необходимо вводить дату в формате {0} или поменять формат в настроках пользователя.',
  	
  	'upload error msg UPLOAD_ERR_INI_SIZE' => 'Загруженый размер файла превышает максимальный допустимый ({0}).',
  	'upload error msg UPLOAD_ERR_FORM_SIZE' => 'Загруженый размер файла превышает максимальный допустимый ({0}).',
  	'upload error msg UPLOAD_ERR_PARTIAL' => 'Файл был загружен частично.',
  	'upload error msg UPLOAD_ERR_NO_FILE' => 'Файл не может быть загружен.',
  	'upload error msg UPLOAD_ERR_NO_TMP_DIR' => 'Файл не может быть загружен, ошибка временной папки.',
  	'upload error msg UPLOAD_ERR_CANT_WRITE' => 'Невозможно записать файл на диск.',
  	'upload error msg UPLOAD_ERR_EXTENSION' => 'Расширение PHP остановило загрузку файлов.',
  ); // array

?>