<?php

// Return array of langs
return array(
	'chelp pending tasks widget' => 'Этот виджет отображает ваши незаконченные задачи. Вы можете просмотреть только незаконченные задачи, возложенные на вас, или вы можете изменить эту настройку, зайдя на эту панель инструментов.',
	'chelp documents widget' => 'Этот виджет отображает последние документы в выбранном проекте.',
 	'chelp active tasks widget' => 'Этот виджет отображает ваши активные и приостановленные задачи.',
 	'chelp late tasks widget' => 'Этот виджет отображает ваши последние задачи и этапы.',
 	'chelp calendar widget' => 'Этот виджет отображает события, задачи и этапы на текущую неделю. Вы можете создать новое событие, нажав на один день.',
 	'chelp comments widget' => 'Этот виджет отображает последние комментарии для объектов в текущем проекте.',
 	'chelp dashboard info widget' => 'Этот виджет отображает информацию о текущем проекте, включая пользователей имеющих доступ, присвоенных контактов, и т.д.',
 	'chelp emails widget' => 'Этот виджет отображает последние email сообщения в текущем проекте.',
 	'chelp messages widget' => 'Этот виджет показывает последние заметки для выбранного проекта.',
 	
 	'chelp active tasks panel' => 'Ниже отображаются все ваши активные задачи, независимо от текущего проекта. Здесь вы можете остановить или запустить в работу необходимые задачи.',
	'chelp general timeslots panel' => 'Эта панель отображает общее затраченное время на проект/задачу',

	'chelp personal account' => 'Это ваша персональная запись.<br/>Здесь вы можете обновить ваш профиль и аватар, изменить свой пароль и редактировать ваши личные настройки.',
	'chelp user account' => 'Эта учетная запись пользователя.',
	'chelp user account admin' => 'Эта учетная запись пользователя.<br/>Как администратор, вы можете обновить пользовательский профиль и аватар, изменить его пароль и настройки.',

	'chelp reporting panel' => '<b>Добро пожаловать в Панель отчетов</b><br/>Вы можете настроить отчеты от различных объектов, расположенных на левой панели.',
	'chelp reporting panel manage' => '<b>Добро пожаловать в Панель отчетов</b><br/>Вы можете настроить отчеты от различных объектов, расположенных на левой панели.<br/>Можете управлять привилегиями, вы можете создавать и управлять отчетами для каждого типа объекта.',
	'chelp reporting panel manage admin' => '<b>Добро пожаловать в Панель отчетов</b><br/>Вы можете настроить отчеты от различных объектов, расположенных на левой панели.<br/>Можете управлять привилегиями, вы можете создавать и управлять отчетами для каждого типа объекта. Настройки можно устанавливать для каждого пользователя в разделе пользователи.',

	'chelp addfile' => 'You are given the option to upload a file or a web link.<br/>
						<b> File:</b> Allows you to search and upload any document from you local hard drive.<br/>
						<b> Web link:</b> Allows you to enter the url of a web site you want to make reference to.<br/>',

 	'remove context help' => 'Удалить это сообщение',

	'chelp tasks list welcome' => 'Добро пожаловать на панель задач',

	'chelp tasks list' => '<br/>You can apply one of the following filters:<br/>
							<b>Created by</b>: only display tasks created by a specific user or group <br/>
							<b>Completed by</b>: only display tasks completed by a specific user or group <br/>
							<b>Assigned to</b>: only display tasks assigned to a specific user or group <br/>
							<b>Assigned by</b>: only display tasks assigned by a specific user or group <br/>
							<b>Milestone</b>: only display tasks belonging to a specific milestone<br/>
							<b>Priority</b>: only display tasks with a specific priority (low, normal, high) <br/>
							</td>
							<td>
							<br/>You can group tasks by the following criteria (among others):<br/>
							<b>Milestone</b>: group tasks by a specific milestone <br/>
							<b>Priority</b>: group tasks by a specific priority (low, normal, high) <br/>
							<b>Workspace</b>: group tasks by a specific workspace <br/>',
	
	'help opt show' => 'Включить',
	'help opt hide' => 'Выключить',
	'context help messages' => 'Контекстная справка',
	'enable all chelp messages' => 'Включить все контекстные сообщения, которые были отключены вручную',
	'enable all chelp messages click' => 'нажмите тут',
	'help manual' => 'Справка',
	'about us' => 'О нас',
	'success enable all context help' => 'Контекстная справка успешно перезапущена.',
	'success enable context help' => 'Контекстная справка успешно запущена.',
	'success disable context help' => 'Контекстная справка успешно отключена.',
	'how to purchase' => 'Как приобрести',
	'how to purchase desc' => 'Информация о наших различных тарифах и преимуществах', 
	'add ticket' => 'Добавить новый запрос в службу поддержки',
	'add ticket desc' => "Получить персональную помощь по Feng Office",	
	
); // array

?>