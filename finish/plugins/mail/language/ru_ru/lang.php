<?php return array(
	'mail' => 'Почта',
	'linked mail tab'=>'Письма',
    'subscribed notification mail' => 'Вы подписанны на рассылку {0}',
    'subscribed notification mail desc' => '{1} подписал Вас на рассылку {0}. ',
	'user config option name mails_per_page' => 'Писем на странице',
  	'user config option desc mails_per_page' => 'Количество писем в списке (избегайте 0 или отрицательные числа), чем больше число, тем больше времени потребуется, чтобы загрузить электронную почту',
);
