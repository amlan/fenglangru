<?php return array(
	"field workspace description" => "Описание",
	"workspace description" => "Описание проекта",
	"field workspace show_description_in_overview" => "Показать описание проекта на вкладке Обзор?",

	'add new relation workspaces' => 'Выбрать проект',
	'add new relation tags' => 'Присвоить тег',

	'add new workspace' => 'Добавить новой проект',
	'add your first workspace' => 'Добавить проект',
	'you have no workspaces yet' => 'У Вас нет еще проекта',

	'filter by workspaces' => 'Фильтр по проекту',
	'filter by tags' => 'Фильтр по тегу',

	'user config option name lp_dim_workspaces_show_as_column' => 'Отобразить проекты, в отдельном столбце',
    'user config option desc lp_dim_workspaces_show_as_column' => 'Показать проекты в отдельном столбце, а не рядом с колонкой имя',
	'user config option name lp_dim_tags_show_as_column' => 'Отобразит теги, в отдельном столбце',
    'user config option desc lp_dim_tags_show_as_column' => 'Показать теги в отдельном столбце, а не рядом с колонкой имя',
);
